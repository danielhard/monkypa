class User {
  int id;
  String email;
  String first_name;
  String last_name;
  String avatar;

  User({this.id, this.email, this.first_name, this.last_name, this.avatar});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'email': email,
      'first_name': first_name,
      'last_name': last_name,
      'avatar': avatar,
    };
  }

}
