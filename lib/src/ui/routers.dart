import 'package:flutter/material.dart';
import 'package:monkey/src/ui/homePage.dart';
import 'package:monkey/src/ui/users.dart';

class Routes {
  static const String homePage = HomePage.routeName;
  static const String usersPage = UsersPage.routeName;


  static getRouters(BuildContext context) {
    return {
      homePage: (context) => HomePage(),
      usersPage: (context) => UsersPage()
    };
  }
}