import 'package:custom_radio_grouped_button/CustomButtons/ButtonTextStyle.dart';
import 'package:custom_radio_grouped_button/CustomButtons/CustomRadioButton.dart';
import 'package:flutter/material.dart';
import 'package:monkey/src/ui/sections/result.dart';
import 'package:monkey/src/ui/widgets/buttonPrimary.dart';
import 'package:monkey/src/ui/widgets/inputText.dart';

class Palante extends StatefulWidget {
  @override
  _PalanteState createState() => _PalanteState();
}

class _PalanteState extends State<Palante> {
  final _formkey = GlobalKey<FormState>();
  TextEditingController monto = TextEditingController();
  TextEditingController plazo = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.blueAccent),
      child: Padding(
        padding: const EdgeInsets.only(left: 30.0, right: 30.0),
        child: Column(
          children: [
            AppBar(
              backgroundColor: Colors.blueAccent,
              elevation: 0,

            ),
            Image.asset(
              'assets/group.png',
              fit: BoxFit.fill,
            ),
            Center(
              child: Text(
                "Es una plataforma para conectar gente y facilitar microcréditos conectando a quienes necesitan dinero, con quienes pueden prestarlo y obtener ingresos por ello.",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.w600),
              ),
            ),
            SizedBox(height: 30),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image.asset(
                  'assets/group-3.png',
                  fit: BoxFit.fill,
                ),
                ButtonPrimary(
                  colorPrimary: Colors.white,
                  colorOnPrimary: Colors.blueAccent,
                  colorOnSurface: Colors.grey,
                  textButton: 'Otorgantes',
                  onPressedDynamicButton: () {
                    //function
                  },
                )
              ],
            ),
            SizedBox(height: 30),
            Divider(
              color: Colors.white,
            ),
            SizedBox(height: 10),
            Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30)),
                elevation: 10,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(30),
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(25),
                        child: Text(
                          'Calcular crédito',
                          style: TextStyle(
                              color: Colors.blueAccent,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: _tabSection(context),
                      ),
                    ],
                  ),
                )),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }

  Widget _tabSection(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            child: TabBar(labelColor: Colors.blueAccent, tabs: [
              Tab(
                text: "Tomador",
              ),
              Tab(text: "Otorgante"),
            ]),
          ),
          Container(
            //Add this to give height
            height: 280,
            child: TabBarView(children: [
              Container(
                child: Form(
                  key: _formkey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 10),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text('Elige el monto:'),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: SizedBox(
                            height: 70,
                            child: InputText(
                              placeholder: 'monto',
                              helperText: "Max 1'000.000 ",
                              keyboardType: TextInputType.number,
                              textEditingController: monto,
                              validator: (value) {
                                if (value.isNotEmpty && value.length >= 3 && value.length <= 7) {
                                  monto.text = value;
                                  return null;
                                }
                                return "información no validad";
                              },
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text('Elige el Plazo de la cuotas:'),
                      ),
                      CustomRadioButton(
                        elevation: 0,
                        //absoluteZeroSpacing: false,
                        enableButtonWrap: true,
                        wrapAlignment: WrapAlignment.spaceBetween,
                        unSelectedColor: Theme.of(context).canvasColor,
                        enableShape: true,
                        buttonLables: [
                          '3 meses',
                          '6 meses',
                        ],
                        buttonValues: [
                          "3",
                          "6",
                        ],
                        buttonTextStyle: ButtonTextStyle(
                            selectedColor: Colors.white,
                            unSelectedColor: Colors.black,
                            textStyle: TextStyle(fontSize: 16)),
                        radioButtonValue: (value) {
                          print(value);
                          plazo.text = value;
                        },
                        selectedColor: Theme.of(context).accentColor,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Center(
                        child: ButtonPrimary(
                          colorPrimary: Colors.blueAccent,
                          colorOnPrimary: Colors.white,
                          colorOnSurface: Colors.grey,
                          textButton: 'Calcular',
                          onPressedDynamicButton: () {
                            _sendForm();
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                child: Center(child: Text("Informacion del tomador")),
              ),
            ]),
          ),
        ],
      ),
    );
  }

  void _sendForm() {
    final isValid = _formkey.currentState.validate();
    if(isValid && monto != null) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => Result(
                monto: monto.text,
                plazo: plazo.text,
              )));
    }
  }

}
