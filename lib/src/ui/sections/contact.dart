import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:monkey/src/ui/widgets/buttonPrimary.dart';
import 'package:monkey/src/ui/widgets/inputText.dart';

class Contact extends StatefulWidget {
  @override
  _ContactState createState() => _ContactState();
}

class _ContactState extends State<Contact> {
  final _formkeyMail = GlobalKey<FormState>();
  TextEditingController email = TextEditingController();
  TextEditingController message = TextEditingController();
  List<String> attachments = [];
  bool isHTML = false;

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(color: Colors.deepPurpleAccent),
        child: Padding(
            padding: const EdgeInsets.only(left: 30.0, right: 30.0),
            child: Form(
              key: _formkeyMail,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 20),
                    Center(
                      child: Text(
                        "Contacto",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 40,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    SizedBox(height: 60),
                    Center(
                      child: Text(
                        "información@creditospalente.com",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            decoration: TextDecoration.underline),
                      ),
                    ),
                    SizedBox(height: 10),
                    Center(
                      child: Text(
                        "lina.rojas@creditospalente.com",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            decoration: TextDecoration.underline),
                      ),
                    ),
                    SizedBox(height: 70),
                    Text(
                      'Correo:',
                      style: TextStyle(color: Colors.white),
                    ),
                    SizedBox(height: 10),
                    SizedBox(
                        height: 70,
                        child: InputText(
                          placeholder: 'correo@correo.com',
                          textEditingController: email,
                          keyboardType: TextInputType.emailAddress,
                          validator: (value) {
                            if (value.isNotEmpty && value.length >= 3 && !value.contains(' ') && value.contains("@") && value.contains(".")) {
                              email.text = value;
                              return null;
                            }
                            return "Correo no valido";
                          },
                        )),
                    SizedBox(height: 10),
                    Text(
                      'Mensaje:',
                      style: TextStyle(color: Colors.white),
                    ),
                    SizedBox(height: 10),
                    SizedBox(
                        child: InputText(
                      placeholder: 'mensaje',
                      maxLines: 10,
                      textEditingController: message,
                          keyboardType: TextInputType.text,
                          validator: (value) {
                            if (value.isNotEmpty && value.length >= 3 ) {
                              message.text = value;
                              return null;
                            }
                            return "Correo no valido";
                          },
                    )),
                    SizedBox(height: 10),
                    Center(
                        child: ButtonPrimary(
                      colorPrimary: Colors.white,
                      colorOnPrimary: Colors.deepPurpleAccent,
                      colorOnSurface: Colors.grey,
                      textButton: 'Enviar',
                          onPressedDynamicButton: () {
                            _sendFormMail();
                          },
                    )),
                    SizedBox(height: 10),
                  ]),
            )));
  }


  Future<void> _sendFormMail() async {
    final isValid = _formkeyMail.currentState.validate();
    if(isValid) {
      final Email emaill = Email(
        body: message.text,
        subject: 'Test Monky',
        recipients: [email.text],
        attachmentPaths: attachments,
        isHTML: isHTML,
      );

      String platformResponse;

      try {
        await FlutterEmailSender.send(emaill);
        platformResponse = 'success';
      } catch (error) {
        platformResponse = error.toString();
      }

      if (!mounted) return;
    }

  }

}
