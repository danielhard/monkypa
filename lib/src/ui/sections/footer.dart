import 'package:flutter/material.dart';

class Footer extends StatefulWidget {
  @override
  _FooterState createState() => _FooterState();
}

class _FooterState extends State<Footer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.blueAccent),
      child: Padding(
        padding: const EdgeInsets.only(left: 30.0, right: 30.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Center(child:  Image.asset('assets/group.png',fit: BoxFit.fill,),),
            Text("2021",style: TextStyle(color: Colors.white),),
            Text("By Juan Daniel Peralta",style: TextStyle(color: Colors.white),),
          ],
        ),
      ),

    );
  }
}
