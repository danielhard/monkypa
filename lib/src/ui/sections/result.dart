import 'package:flutter/material.dart';
import 'package:monkey/src/ui/widgets/buttonPrimary.dart';
import 'package:monkey/src/ui/widgets/infoResult.dart';


class Result extends StatefulWidget {
  final String monto,plazo;
  const Result({Key key, this.monto = '0', this.plazo = '1'}) : super(key: key);@override
  _ResultState createState() => _ResultState();
}

class _ResultState extends State<Result> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Pa`lante'),),
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(color: Colors.blueAccent),
          child: Center(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Card(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
                    elevation: 10,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(30),
                        child: Column(
                          children: <Widget>[
                            SizedBox(height: 25,),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                Container(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [

                                      Text('Monto:'),
                                      ButtonPrimary(
                                        colorPrimary: Colors.white,
                                        colorOnPrimary: Colors.blueAccent,
                                        colorOnSurface: Colors.grey,
                                        textButton: this.widget.monto,
                                        onPressedDynamicButton: () {

                                        },
                                      )
                                    ],
                                  ),
                                ),
                                  Container(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text('Plazo:'),
                                      ButtonPrimary(
                                        colorPrimary: Colors.white,
                                        colorOnPrimary: Colors.blueAccent,
                                        colorOnSurface: Colors.grey,
                                        textButton: '${this.widget.plazo} meses',
                                      )
                                    ],
                                  ),
                                ),


                              ],),
                            ),
                            SizedBox(height: 20,),
                            ButtonPrimary(
                              colorPrimary: Colors.blueAccent,
                              colorOnPrimary: Colors.white,
                              colorOnSurface: Colors.grey,
                              textButton: 'Nuevo Cálculo',
                              onPressedDynamicButton: () {
                                Navigator.pop(context);
                              } ,
                            ),
                            SizedBox(height: 20,),
                            Container(
                              decoration: BoxDecoration(color: Colors.grey[100]),
                              child: Column(
                                children: [
                                  SizedBox(height: 20,),
                                  Text(this.widget.monto,style: TextStyle(color: Colors.blueAccent,fontSize: 40),),
                                  SizedBox(height: 10,),
                                  Text('valor a pagar',style: TextStyle(color: Colors.blueAccent),),
                                  SizedBox(height: 10,),
                                  Image.asset('assets/group-3.png',fit: BoxFit.fill,),
                                  Text('Solicitar crédito',style: TextStyle(decoration: TextDecoration.underline,fontWeight: FontWeight.bold),),
                                  SizedBox(height: 10,),
                                  Divider(
                                    color: Colors.grey,
                                  ),
                                  Text('Resumen',style: TextStyle(color: Colors.blueAccent,fontWeight: FontWeight.bold)),
                                  Divider(
                                    color: Colors.grey,
                                  ),
                                  InfoResult(label1: 'Interés (25% E.A):',label2: '4,692',),
                                  InfoResult(label1: 'Seguro:',label2: '1.000',),
                                  InfoResult(label1: 'Administración:',label2: '16.000',),
                                  Center(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Text('Subtotal:',style: TextStyle(color: Colors.blueAccent,fontWeight: FontWeight.bold),),
                                        Text(this.widget.monto,style: TextStyle(color: Colors.blueAccent,fontWeight: FontWeight.bold)),
                                      ],
                                    ),
                                  ),
                                  Divider(
                                    color: Colors.grey,
                                  ),
                                  Center(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                                      children: [
                                        Text('IVA:',),
                                        Text('564.343',style: TextStyle(fontWeight: FontWeight.bold)),
                                      ],
                                    ),
                                  ),
                                  InfoResult(label1: 'Tecnologia:',label2: '16.000',),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
