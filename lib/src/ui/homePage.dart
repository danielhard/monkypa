import 'package:flutter/material.dart';
import 'package:monkey/src/ui/sections/contact.dart';
import 'package:monkey/src/ui/sections/footer.dart';
import 'package:monkey/src/ui/sections/palante.dart';
import 'package:monkey/src/ui/users.dart';
class HomePage extends StatefulWidget {
  static const String routeName = "/homePage";
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          children: [
            ListTile(
              title: Text("Usuarios Api"),
              onTap: (){
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => UsersPage(
                        )));
              },
            )
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Palante(),
            Contact(),
            Footer()
          ],
        ),
      ),
    );
  }
}


