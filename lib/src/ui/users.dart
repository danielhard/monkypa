import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:monkey/db/configDB.dart';
import 'package:monkey/src/models/user.dart';
import 'dart:convert';
import 'package:monkey/src/ui/widgets/buttonPrimary.dart';

class UsersPage extends StatefulWidget {
  static const String routeName = "/usersPage";
  @override
  _UsersPageState createState() => _UsersPageState();
}

class _UsersPageState extends State<UsersPage> {
  int pageNumber = 1;

  Future<List<User>> _getUsers() async {
    var data = await http.get('https://reqres.in/api/users?page=$pageNumber');
    var jsonData = json.decode(data.body);
    List<User> users = [];
    for (var u in jsonData['data']) {
      User user = User(id: u["id"],email: u["email"],first_name: u["first_name"],last_name: u["last_name"],avatar:u["avatar"]);
      users.add(user);
    }

    print(users.length);
    return users;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Usuarios'),
      ),
      body: FutureBuilder(
          future: _getUsers(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            print(snapshot.data);
            if (snapshot.data == null) {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            } else {
              return Stack(
                children: [
                  ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return ListTile(
                          leading: CircleAvatar(
                            backgroundImage:
                                NetworkImage(snapshot.data[index].avatar),
                          ),
                          title: Text(snapshot.data[index].first_name +
                              ' ' +
                              snapshot.data[index].last_name),
                          subtitle: Text(snapshot.data[index].email),
                        );
                      }),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      margin:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 40),
                      width: double.infinity,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          ButtonPrimary(
                            colorPrimary: Colors.white,
                            colorOnPrimary: Colors.deepPurpleAccent,
                            colorOnSurface: Colors.grey,
                            textButton: '1',
                            onPressedDynamicButton: () {
                              setState(() {
                                pageNumber = 1;
                              });
                            },
                          ),
                          ButtonPrimary(
                            colorPrimary: Colors.white,
                            colorOnPrimary: Colors.deepPurpleAccent,
                            colorOnSurface: Colors.grey,
                            textButton: '2',
                            onPressedDynamicButton: () {
                              setState(() {
                                pageNumber = 2;
                              });
                            },
                          ),
                          ButtonPrimary(
                            colorPrimary: Colors.deepPurpleAccent,
                            colorOnPrimary: Colors.white,
                            colorOnSurface: Colors.grey,
                            textButton: 'Guardar',
                            onPressedDynamicButton: ()  {

                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              );
            }
          }),
    );
  }
}
