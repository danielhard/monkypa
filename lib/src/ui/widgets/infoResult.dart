import 'package:flutter/material.dart';

class InfoResult extends StatefulWidget {
  final String label1,label2;


  const InfoResult({Key key, this.label1, this.label2}) : super(key: key);@override
  _InfoResultState createState() => _InfoResultState();
}

class _InfoResultState extends State<InfoResult> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left:40.0,right: 40.0,top: 8.0,bottom: 8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Icon(
            Icons.info_outline,
            color: Colors.blueAccent,
            size: 24.0,
          ),
          Text(this.widget.label1,style: TextStyle(color: Colors.grey),),
          Text(this.widget.label2,style: TextStyle(fontWeight: FontWeight.bold),),
        ],
      ),
    );
  }
}
