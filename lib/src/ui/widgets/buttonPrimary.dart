import 'package:flutter/material.dart';

class ButtonPrimary extends StatefulWidget {
  final Color colorPrimary;
  final Color colorOnPrimary;
  final Color colorOnSurface;
  final String textButton;
  final VoidCallback onPressedDynamicButton;

  const ButtonPrimary(
      {Key key, this.textButton, this.onPressedDynamicButton, this.colorPrimary, this.colorOnPrimary, this.colorOnSurface})
      : super(key: key);
  @override
  _ButtonPrimaryState createState() => _ButtonPrimaryState();
}

class _ButtonPrimaryState extends State<ButtonPrimary> {
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      child: Text(this.widget.textButton),
        style: ElevatedButton.styleFrom(
          primary: this.widget.colorPrimary,
          onPrimary: this.widget.colorOnPrimary,
          onSurface: this.widget.colorOnSurface,
          shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
        ),
      onPressed: () {
        this.widget.onPressedDynamicButton();
      },
    );
  }
}
