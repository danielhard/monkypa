import 'package:flutter/material.dart';

class InputText extends StatefulWidget {
  final String placeholder,helperText;
  final Function(String text) validator;
  final TextInputType keyboardType;
  final TextEditingController textEditingController;
  final IconData iconPath;
  final int maxLines;

  const InputText({Key key, this.placeholder, this.validator, this.keyboardType, this.textEditingController, this.helperText, this.iconPath, this.maxLines = 1}) : super(key: key);
  @override
  _InputTextState createState() => _InputTextState();
}

class _InputTextState extends State<InputText> {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: new ThemeData(
        primaryColor: Colors.blueAccent,
      ),
      child: TextFormField(
        maxLines: widget.maxLines,
        controller: widget.textEditingController,
        validator: widget.validator,
        keyboardType: widget.keyboardType,
        decoration: InputDecoration(
          helperText: widget.helperText,
          filled: true,
          fillColor: Colors.white,
          focusColor: Colors.black,
          border: new OutlineInputBorder(
              borderRadius: BorderRadius.circular(40.0)
          ),
          hintText: widget.placeholder,
        ),
      ),
    );
  }
}
