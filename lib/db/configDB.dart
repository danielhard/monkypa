import 'package:monkey/src/models/user.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class ConfigDB{
 static Future<Database> _openDB() async {
    return openDatabase(join(await getDatabasesPath(), 'users.db'),
        onCreate: (db, version) {
      return db.execute(
          "CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT, first_name TEXT, last_name TEXT, avatar TEXT)");
    }, version: 1);
  }

 static Future<void> insert(User, user) async {
    Database database = await _openDB();
    return database.insert('users', user.toMap());
  }

 static Future<List<User>> getUser() async {
    Database database = await _openDB();
    final List<Map<String, dynamic>> usersMap = await database.query("users");

    for (var u in usersMap) {}

    return List.generate(
        usersMap.length,
        (i) => User(
            id: usersMap[i]['id'],
            email: usersMap[i]['email'],
            last_name: usersMap[i]['last_name'],
            first_name: usersMap[i]['first_name'],
            avatar: usersMap[i]['first_name']));
  }
}
